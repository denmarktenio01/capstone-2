const Product = require('../models/product')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const user = require('../models/user')


// get ALL, ALL products
module.exports.getAllProducts = () => {
    return Product.find().then((products, err) => {
        if(err){
            return err
        } else {
            return products
        }
    })
}


// get ALL active products
module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then((products, err) => {
        if(err){
            return err
        } else {
            return products
        }
    })
}

// get a product
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            return err
        } else {
            return product
        }
    })
}


// add product (ADMIN ONLY)
module.exports.addProduct = (body) => {
    let product = new Product({
        imageSrc: body.imageSrc,
        name: body.name,
        description: body.description,
        price: body.price,
        isActive: body.isActive
    })
    return product.save().then((newProduct, err) => {
        if(err){
            return err
        } else {
            return newProduct
        }
    })
    

    return Product.findOne({name: product.name}).then(result => {
        if(result === null){
            return product.save().then((newProduct, err) => {
                if(err){
                    return err
                } else {
                    return newProduct
                }
            })
        } else {
            return {
                message: `Duplicate product found.`
            }
        }
    })
}

//update product info (ADMIN ONLY)
module.exports.updateProduct = (productId, params) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            console.log(err)
            return false
        }

        product.imageSrc = params.imageSrc,
        product.name = params.name,
        product.description = params.description,
        product.price = params.price

        return product.save().then((updatedProduct,saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false
            } else {
                return updatedProduct;
            }
        })

        // return Product.findOne({name: product.name}).then(result => {
        //     if(result === null){
        //         return product.save().then((updatedProduct,saveErr) => {
        //             if(saveErr){
        //                 console.log(saveErr);
        //                 return false
        //             } else {
        //                 return updatedProduct;
        //             }
        //         })
        //     } else {
        //         return {
        //             message: `Duplicate product found.`
        //         }
        //     }
        // })
    })
}

// archive/restore product (ADMIN)
module.exports.archiveProduct = (productId) => {
    return Product.findById(productId).then((product,err) => {
        if(err){
            return err
        }
        
        product.isActive = !product.isActive;


        return product.save().then((archivedProduct,saveErr) => {
            if(saveErr){
                return saveErr
            } else {
                return archivedProduct
            }
        })
    })
}