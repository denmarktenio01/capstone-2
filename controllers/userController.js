const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Product = require('../models/product')









module.exports.getUsers = () => {

    return User.find().then((users,err) => {
        
            return (err) ? err : users
        
        
    })
}



// get user details
module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        // reassign the password to undefined
        // so it won't retrieve along with other user data
        user.password = undefined
        return user
    })
}













module.exports.register = (body) => {
    let user = new User({
        firstName: body.firstName,
        lastName: body.lastName,
        mobileNumber: body.mobileNumber,
        email: body.email,
        isAdmin: body.isAdmin,
        password: bcrypt.hashSync(body.password, 10)

    })
    return User.findOne({email: user.email}).then(result => {
        if(result === null){
            return user.save().then((user,err) => {
                return (err) ? false : true
            })
        } else {
            return {
                message: 'Email already associated to an existing account.'
            }
        }
    })


    
}

module.exports.login = (params) => {
    return User.findOne({email: params.email}).then(user => {
        if(user === null){
            return {
                message: `Email does not match any account.`
            }
            
                
            
        }

        const doPasswordsMatch = bcrypt.compareSync(params.password, user.password)

        if(doPasswordsMatch){
            return {accessToken: auth.createAccessToken(user.toObject())}
        } else {
            return {
                message: `Incorrect password.`
            }
        }
    })
}

module.exports.setAdmin = (userId) => {
    return User.findById(userId).then((user,err) => {
        if(err){
            console.log(err)
            return false
        }

        user.isAdmin = true

        return user.save().then((newAdmin, saveErr) => {
            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return newAdmin
            }
        })
    })
}


// add an order to a user
module.exports.addOrder = async (data) => {


    let productSaveStatus = await Product.findOne({_id: data.productId}).then((product,err) => {
        if(err){
            return err
        }

        
        let order = {
            productId: data.productId,
            productName: product.name,
            totalAmount: {
                quantity: data.quantity,
                totalPrice: product.price * data.quantity
            }
        }

        return User.findById(data.userId).then(user => {
            user.orders.push(order);
            return user.save().then((user,err) => {
                if(err){
                    return false
                } else {
                    return true
                }
            })
        })
    })

    let userSaveStatus = await Product.findOne({_id: data.productId}).then(product => {
        product.purchases.push({userId: data.userId})
        return product.save().then((product,err) => {
            if(err){
                return false
            } else {
                return true
            }
        })
    })

    if(productSaveStatus && userSaveStatus){
        return true
    } else {
        return false
    }

    
}


// remove an order
module.exports.removeOrder = (userId, body) => {
    return User.findById(userId).then((user,err) => {
        if(err){
            console.log(err)
            return false
        }

        let orderId = body.orderId

        user.orders = user.orders.filter(order => order.id !== orderId)

        return user.save().then((editedUser, saveErr) => {
            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return editedUser
            }
        })
    })
}

//retrieve authenticated user's orders
module.exports.getOrders = (data) => {
    return User.findById(data.userId).then((user,err) => {
        if(err){
            return err;
        } else {
            if(user.orders.length === 0){
                return {
                    message: `You have no orders.`
                }
            } else {
                return user.orders
            }
        }
        
    })
}






// get all orders (ADMIN ONLY)
module.exports.getAllOrders = () => {
    return User.find({"orders.0": {"$exists": true}, isAdmin: false},{orders: 1, email: 1}).then((users,err) => {
        if(err){
            return err
        } else {
            return users
        }
    })
}

// edit user details
module.exports.editInfo = (userId,body) => {
    return User.findById(userId).then((user, err) => {
        if(err){
            console.log(err)
            return false
        }


        user.firstName = body.firstName,
        user.lastName = body.lastName,
        user.mobileNumber = body.mobileNumber

        return user.save().then((editedUser, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false
            } else {
                return editedUser;
            }
        })

        // return User.findOne({email: user.email}).then(result => {
        //     if(result === null){
        //         return user.save().then((editedUser, saveErr) => {
        //             if(saveErr){
        //                 console.log(saveErr);
        //                 return false
        //             } else {
        //                 return editedUser;
        //             }
        //         })
        //     } else {
        //         return {
        //             message: `That email is taken.`
        //         }
        //     }
        // })
    })
}


// change password
module.exports.changePass = (userId, body) => {
    return User.findById(userId).then((user, err) => {
        if(err){
            console.log(err)
            return false
        }
        const doPasswordsMatch = bcrypt.compareSync(body.currentPassword, user.password)
        let newPassword = body.newPassword
        let verifyPassword = body.verifyPassword
        


        if(doPasswordsMatch){
            if(newPassword === verifyPassword){
                user.password = bcrypt.hashSync(newPassword, 10)
                return user.save().then((user,err) => {
                    return (err) ? false : true
                })
            } else {
                return {
                    message: `Passwords do not match!`
                }
            }
            
        } else {
            return false
        }
        

    })
}