const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

const app = express()
app.use(cors()) // Tells the server that cors is being used by your server
app.use(express.json())
app.use(express.urlencoded({extended: true}));



mongoose.connect('mongodb+srv://admin:admin@b106.38r1k.mongodb.net/Capstone2_ecommerce?retryWrites=true&w=majority', {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false
})

mongoose.connection.once('open', ()=> console.log('Now connected to the database'))

app.use("/products", productRoutes)
app.use("/users", userRoutes)


app.listen(process.env.PORT || 4000, ()=>{
    console.log(`API is now online at port ${process.env.PORT || 4000}`)
})
