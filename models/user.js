const mongoose = require('mongoose')





const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    firstName: {
        type: String,
        required: [true, ' First name is requried.']
    },

    lastName: {
        type: String,
        required: [true, 'Last name is required']
    },
    mobileNumber: {
        type: String,
        required: [true, 'Mobile number is required']
    },
    password: {
        type: String,
        required: [true, "Password is required."]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },

    orders: [{
        purchasedOn: {
            type: Date,
            default: new Date()
        },
        productId: {
            type: String,
            required: [true, 'Product ID is required.']
        },
        productName: {
            type: String,
            required: [true, 'Product name is required.']

        },
        totalAmount: {
            quantity: {
                type: Number,
                default: 1
            },
            totalPrice: {
                type: Number
            }
        }
    }]
})

module.exports = mongoose.model('User', userSchema)