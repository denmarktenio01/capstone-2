const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/productController')
const auth = require('../auth')
const Product = require('../models/product')


//add product (ADMIN ONLY)
router.post('/add',auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.addProduct(req.body).then(result => res.send(result))
})

//get ALL active products
router.get('/', (req,res) => {
    ProductController.getActiveProducts().then(result => res.send(result))
})

// get ALL, i mean ALL Products
router.get('/all', auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.getAllProducts().then(result => res.send(result))
})

//get a product
router.get('/:id', auth.verify, (req,res) => {
    ProductController.getProduct(req.params.id).then(result => res.send(result))
})

//update product info (ADMIN)
router.put('/:id/update', auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.updateProduct(req.params.id,req.body).then(result => res.send(result))
})

// archive product (ADMIN)
router.delete('/:id/archive', auth.verify, auth.checkIsAdmin, (req,res) => {
    ProductController.archiveProduct(req.params.id).then(result => res.send(result))
})













module.exports = router