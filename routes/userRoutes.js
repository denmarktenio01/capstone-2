const express = require('express')
const router = express.Router()
const UserController = require('../controllers/userController')
const auth = require('../auth')
const User = require('../models/user')

//gets all users (ADMIN ONLY)
router.get('/', auth.verify, auth.checkIsAdmin,(req,res) => {
    UserController.getUsers().then(result => res.send(result))
})

// get a user's details
router.get('/details', auth.verify, (req,res)=>{
    const user = auth.decode(req.headers.authorization)
    UserController.get({userId: user.id}).then(user => {
        res.send((user))
    })
})


// edit user Information
router.put('/editDetails',auth.verify, (req,res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.editInfo(user.id,req.body).then(result => res.send(result))
})


// change Password
router.put('/changePassword', auth.verify, (req,res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.changePass(user.id,req.body).then(result => res.send(result))
})



//add user
router.post('/register', (req,res) => {
    UserController.register(req.body).then(result => res.send(result))
})

//login user
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => {
        res.send(result)
    })
})

//set user as admin (ADMIN ONLY)
router.put('/grantAdminRights/:id', auth.verify, auth.checkIsAdmin, (req,res) => {
    UserController.setAdmin(req.params.id).then(result => res.send(result))
})

//add an order to a user
router.post('/order', auth.verify, auth.checkNonAdmin, (req,res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        productId: req.body.productId,
        quantity: req.body.quantity
    }
    UserController.addOrder(data).then(result => res.send(result))
})

// remove an order
router.put('/myOrders/remove', auth.verify, (req,res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.removeOrder(user.id,req.body).then(result => res.send(result))
})


// retrive authenticated user's orders
router.get('/myOrders',auth.verify, auth.checkNonAdmin, (req,res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id
    }
    UserController.getOrders(data).then(result => res.send(result))
})


// retrieve ALL orders (ADMIN ONLY)
router.get('/orders', auth.verify, auth.checkIsAdmin, (req,res) => {
    UserController.getAllOrders().then(result => res.send(result))
})










module.exports = router;